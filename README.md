# MicroServiceLib

**TODO: Add description**

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed as:

  1. Add micro_service_lib to your list of dependencies in `mix.exs`:

        def deps do
          [{:micro_service_lib, "~> 0.0.1"}]
        end

  2. Ensure micro_service_lib is started before your application:

        def application do
          [applications: [:micro_service_lib]]
        end

