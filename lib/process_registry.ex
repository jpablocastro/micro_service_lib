defmodule MicroServiceLib.ProcessRegistry do

  def join({:ok, pid}, process_group) do
    :pg2.create(process_group)
    :pg2.join(process_group, pid)
    {:ok, pid}
  end

  def get_members(process_group) do
    :pg2.get_local_members(process_group)
  end

  def random_member(process_group) do
    get_members(process_group)
    |> Enum.shuffle
    |> hd
  end

end
