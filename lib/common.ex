defmodule MicroServiceLib.Common do

  def random_reference do
    :crypto.strong_rand_bytes(12)
    |> :base64.encode_to_string
    |> to_string
  end
end

