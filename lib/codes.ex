defmodule MicroServiceLib.Codes do
  #client error
  def for(:unauthorized),        do: 401 # Requires authentication/authentication is wrong
  def for(:bad_request),         do: 400 # Malformed request
  def for(:not_found),           do: 404 # Resource not found
  def for(:conflict),            do: 409 # Conflict with existing entity
  #success
  def for(:ok),                  do: 200 # OK
  def for(:created),             do: 201 # Resource created
  def for(:no_content),          do: 204 # No response returned, for example on delete
  #server error
  def for(:server_error),        do: 500 # Error
  def for(:bad_gateway),         do: 502 # Bad Gateway
  def for(:service_unavailable), do: 503 # Service Unavailable
  def for(:timeout),             do: 504 # Gateway Timeout
end
