defmodule MicroServiceLib.Service do

  def call(pid, message, payload) do
    GenServer.call(pid, {message, payload})
  end

  def call(pid, message, payload, timeout) do
    GenServer.call(pid, {message, payload}, timeout)
  end

end
