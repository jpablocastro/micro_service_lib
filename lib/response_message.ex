defmodule MicroServiceLib.ResponseMessage do

  defstruct success: false, code: 99, errors: [], result: nil

  def build(%{valid?: false} = changeset) do
    errors = MicroServiceLib.ChangesetErrors.parse(changeset)
    error_code = select_error_code(errors)
    build(error_code, errors)
  end

  def build(code, errors, result \\ nil)

  def build(code, [], result), 
    do: %__MODULE__{success: true,
                    code:    code, 
                    result:  result}

  def build(code, nil, result), 
    do: %__MODULE__{success: true,
                    code:    code, 
                    result:  result}

  def build(code, errors, result), 
    do: %__MODULE__{success: false, 
                    errors:  errors, 
                    code:    code, 
                    result:  result}

    defp select_error_code(errors) do
      Enum.reduce(errors, MicroServiceLib.Codes.for(:bad_request), fn([_, message], code) ->
        cond do
          Regex.match?(~r/has already been taken/, message) ->
            MicroServiceLib.Codes.for(:conflict)

          Regex.match?(~r/does not exist/, message) ->
            MicroServiceLib.Codes.for(:conflict)

          true ->
            code
        end
      end)
    end

  def clear_not_loaded_associations(nil), do: nil

  def clear_not_loaded_associations(%{entries: entries} = result) do
    new_entries = Enum.map(entries, fn(e) -> clear_not_loaded_associations(e) end)
    %{result | entries: new_entries}
  end

  def clear_not_loaded_associations(result) do
    Enum.reduce(result, %{}, fn({k,v}, new_result) ->
      case v do
        %{__struct__: Ecto.Association.NotLoaded} ->
          new_result
        _ -> 
          Map.put(new_result, k, v)
      end
    end)
  end

  def swagger_schema, 
    do: %{type: :object, 
          properties: %{
            success: %{
              type: :boolean
            },
            code: %{
              type: :integer
            },
            errors: %{
              type: :array,
              items: %{
                type: :array,
                items: %{ 
                  type: :string
                }
              }
            },
            result: %{
              type: :object
            }
          }
        }

  def swagger_schema(result_schema), 
    do: %{type: :object, 
          properties: %{
            success: %{
              type: :boolean
            },
            code: %{
              type: :integer
            },
            errors: %{
              type: :array,
              items: %{
                type: :array,
                items: %{ 
                  type: :string
                }
              }
            },
            result: %{
              type: :object,
              properties: result_schema
            }
          }
        }

end
