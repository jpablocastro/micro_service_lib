defmodule MicroServiceLib.Atomizer do
  def atomize(map, keys) do
    Enum.map(map, fn({k,v}) -> 
      {get_atom(k, keys), v}
    end)
    |> Enum.reduce(%{}, fn({k,v}, acc) ->
      Map.put(acc, k, v)
    end)
  end

    defp get_atom(key, keys) do
      possible =
        Enum.filter(keys, fn({_a, k}) -> 
          key == k
        end)

      case possible do
        [] -> key
        [{a, _k} | _t] -> a
      end

  end
end
