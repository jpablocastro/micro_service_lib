defmodule MicroServiceLib.WorkerSupervisor do
  use Supervisor

  def start_link({count, module, params, options}) do
    Supervisor.start_link(__MODULE__, [{count, module, params, options}], [id: make_ref])
  end

  def init([{count, module, params, options}]) do
    children =
      Enum.map(1..count, fn(_) -> 
        worker(module, params, options ++ [id: make_ref])
      end)
    supervise(children, strategy: :one_for_one)
  end
end
