defmodule MicroServiceLib.ChangesetErrors do
  def parse(%{valid?: true}) do
    []
  end

  def parse(%{valid?: false, errors: errors}) do
    Enum.map(errors, fn({field, {error, _}}) -> 
      [field, error]
    end)
  end
end
